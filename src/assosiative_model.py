# coding: utf-8

# writer: Takuya Togo

import math
import numpy as np
import matplotlib.pyplot as plt
import sys

class RandomMemoryMaker:
    @staticmethod
    def make(n=1000, m=80):
        return np.random.choice([-1, 1], (m, n))

class AssosiativeModel:
    def __init__(self,
                memory
                ):

        # self._memory = memory
        n = len(memory[0])
        m = len(memory)

        if n == 0:
            print('Error: n == 0', file=sys.stderr)
            return

        self._x = np.zeros(n)
        self._weight = self._set_weight(memory, n, m)

    def _set_weight(self, memory, n, m):
        self._w = np.zeros((n, n))
        for i in range(m):
            self._w += (np.reshape(memory[i], (1, n)) * np.reshape(memory[i], (n, 1)))
        self._w /= n

        # 対角成分を0にする
        self._w -= self._w * np.identity(n)

    def _active(self):
        """
        Activity Dynamics１回分
        """
        n = len(self._x)
        x = np.zeros(n)
        for i in range(n):
            x[i] = np.where(np.dot(self._w[i], self._x) > 0, 1, -1)

        self._x = x

    def _cos(self, x1, x2):
        # return np.dot(x1, x2) / (np.linalg.norm(x1) * np.linalg.norm(x2))
        return np.dot(x1, x2) / len(x1)
    
    def assosiate(self, initial_x, target_x, looptime):
        direction_cosine = []
        self._x = initial_x
        
        direction_cosine.append(self._cos(self._x, target_x))

        for i in range(looptime):
            self._active()
            direction_cosine.append(self._cos(self._x, target_x))
        
        return direction_cosine

if __name__ == '__main__':
    x = RandomMemoryMaker.make()
    assosiative_model = AssosiativeModel(x)
