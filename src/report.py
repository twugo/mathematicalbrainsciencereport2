# coding: utf-8

# writer: Takuya Togo

import numpy as np
import matplotlib.pyplot as plt
import assosiative_model

class Printer:
    @staticmethod
    def plot_figure(data, title, filename = "out.png", xlabel="iteration", ylabel="E"):
        fig = plt.figure()
        plt.title(title)
        # plt.scatter(range(1, len(data)+1), data, s = 1, marker=".", )
        # plt.plot(range(0, len(data)), data)
        plt.plot(data)
        

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.savefig(filename)

    @staticmethod
    def scatter_figure(data, title, filename = "out.png", xlabel="iteration", ylabel="E"):
        fig = plt.figure()
        plt.title(title)
        plt.scatter(range(1, len(data)+1), data, s = 1, marker=".", )

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.savefig(filename)

class Solver:
    @staticmethod
    def flip(x, a):
        result = np.copy(x)
        for i in range(a):
            result[i] *= -1

        return result

    # @staticmethod
    # def solve(a):
    #     x = assosiative_model.RandomMemoryMaker.make()
    #     model = assosiative_model.AssosiativeModel(x)

    #     x0 = Solver.flip(x[0], a)
    #     return model.assosiate(x0, 20)       

class Report:
    def q1(self):
        x = assosiative_model.RandomMemoryMaker.make()
        model = assosiative_model.AssosiativeModel(x)
        fig = plt.figure()
        xlabel = "time"
        ylabel="direction cosine"

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        
        for i in range(0, 1000, 25):
            x0 = Solver.flip(x[0], i)
            data = model.assosiate(x0, x[0], 20)
            print(i)
            plt.plot(data)

        filename = "out/x0.png"
        plt.savefig(filename)

    def parallel(self):
        x = assosiative_model.RandomMemoryMaker.make()
        model = assosiative_model.AssosiativeModel(x)
        
        for i in range(0, 1000, 25):
            x0 = Solver.flip(x[0], i)
            data = model.assosiate(x0, x[0], 20)
            print(data)
            filename="out/x0_a" + str(i) + ".png"
            Printer.plot_figure(data, title="", xlabel="time", ylabel="direction cosine")
            print(i)

def main():
    np.random.seed(0)
    report = Report()
    report.q1()
    # report.parallel()

if __name__ == '__main__':
    main()
